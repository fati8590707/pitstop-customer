const fs = require('fs');

module.exports = {
    development: {
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      host: process.env.DB_HOST,
      dialect: 'mysql',
      logging: false,
      dialectOptions: {
        bigNumberStrings: true
      }
    },
    test: {
      username: process.env.CI_DB_USERNAME,
      password: process.env.CI_DB_PASSWORD,
      database: process.env.CI_DB_NAME,
      host: '127.0.0.1',
      port: 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true
      }
    },
    production: {
      username: process.env.PROD_DB_USERNAME || 'root',
      password: process.env.PROD_DB_PASSWORD || null,
      database: process.env.PROD_DB_NAME || 'database_production',
      host: process.env.PROD_DB_HOSTNAME || 'customer-database',
      port: process.env.PROD_DB_PORT || 3306,
      dialect: 'mysql',
      dialectOptions: {
        bigNumberStrings: true,
      }
    }
  };